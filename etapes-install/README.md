---
title: Local Deploy UniswapV2
date: Feb. 2023
author:  
- MR
---

teste sur  :

- Intel MacOS 12.6 Monterey

- VM GNU/Linux
  - VM Mint 21.1 tout passe sauf raw swap à revoir
    - ensipc: script lance-vm.sh créé, simplement à executer depuis /matieres/WMMFMB22 &#x2705;
    - dans le script : option */scratch* pour tout supprimer en fin de TP, *homedir* pour garder jusqu'à la séance suivante
    - ne pas oublier de desactiver USB 2.0 sur les machines de l'école
    - attention aux droits depuis matieres/WMMFMB22
    - add french keyboard
    - max res 1280x960
  - VM bodhi 6.0 plus legère base tests passent


todo :

- architecture du code à détailler avec un [arbre pour l'affichage](https://marketplace.visualstudio.com/items?itemName=Shinotatwu-DS.file-tree-generator)


[[_TOC_]]

<!--PROF
Remarques :

- Il faudra formater les transactions si on veut en donner en input, donc definir le format + temps pour ecrire un script

- comment on fait sans arbitrageurs?
-->

# Pour la version hardhat de UniswapV2 avec Uniswap Core V2

## Installation &#x274E;

- install yarn
- ``yarn test`` ne passe pas callback err: callback already called
- install ``nvm`` et ``node@12``
- ensuite on lance ``npx hardhat node`` et
```
let taskName = parsedTaskName ?? task_names_1.TASK_HELP;
                                       ^
SyntaxError: Unexpected token '?'
```
La solution proposee est de passer a ``node@18`` mais deja ca ne fonctionne pas et en plus ca ca refait planter yarn au depart qui veut ``node@12``

# Pour la version hardhat de UniswapV2 avec Uniswapv2-solc0.8

## Installation &#x2705;
Avec https://github.com/islishude/uniswapv2-solc0.8 c'est quasi direct
- fonctionne avec ``node@18``
- ``npx hardhat compile`` et ``npx hardhat test``
- [a checker mais ca a l'air useless] ~~on ajoute ``uniswap-v2-deploy-plugin`` (https://github.com/AlexBHarley/uniswap-v2-deploy-plugin)~~
    - ~~install ``yarn add uniswap-v2-deploy-plugin``~~
    - ~~import ``import "uniswap-v2-deploy-plugin";`` dans ``hardhat.config.ts``~~
    - ~~copier le dossier ``build`` de la ``v2-core-master`` a la racine du projet pour eviter l'erreur qui ne trouve pas les fichiers ``.json`` depuis ``node_modules/uniswap-v2-deploy-plugin/dist/deployer/UniswapV2Deployer.js``~~

## Deploy &#x2705;

- ``npx hardhat node`` (ou ``yarn``, et option ``--port`` dispo) est l'equivalent de ``ganache-cli`` et genere les paires de cles
- ~~reutiliser ``swap.ts`` de ``uniswap-v2-deploy-plugin`` et revoir les import du fichier~~
- ~~ajouter dans ``ts-config.json`` les chemins custom  ``"paths": { "~/*": ["src/*"] },`` ainsi que ``"skipLibCheck": true``~~
- On recupere le script de deploiement de https://github.com/darwinia-network/dvm-workshop/tree/main/uniswap/smart-contracts dans ``smart-contracts/scripts/deploy-uniswap.js`` [a regarder : https://github.com/wighawag/hardhat-deploy]
- Quelques petites modifs a y faire notamment les noms des artifacts ``WETH`` --> ``WETH9`` et ``UniswapV2Router02`` --> ``UniswapV2Router``
- [Essayer de s'en passer] On recupere le fichier ``Multicall.sol`` dans ``smart-contracts/contracts/``
- Si besoin on ajoute dans ``hardhat.config.ts`` son reseau local notamment si on prevoit de lancer le node sur un custom port (default: 8545)
```js
    localhost: {
      url: "http://127.0.0.1:8544"
      gasPrice: 204266,
      gas: 99064000,
      blockGasLimit: 3000000000000,
      gas: 2100000,
      gasPrice: 9000000000,
    },
```
- [Essayer de s'en passer] Dans ``hardhat.config.ts`` j'ai ajoute aussi ``allowUnlimitedContractSize: true,`` pour eviter tous potentiels les bugs de gasLimit
- On lance hardhat sur le custom port
```bash
npx hardhat node --port 8544
```
- Dans un autre terminal on deploie sur localhost
```js
>$ npx hardhat run scripts/deploy-uniswap.js --network localhost --port 8544
Deploying contracts using 0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266
WETH deployed to : 0x5FbDB2315678afecb367f032d93F642f64180aa3
Factory deployed to : 0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512
Router deployed to :  0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0
Multicall deployed to : 0xCf7Ed3AccA5a467e9e704C703E8D87F634fB0Fc9
```
Comme ganache il va utiliser la premiere adresse pour deployer les contrats
- On peut recuperer une console
```bash
npx hardhat console --network localhost
```
Et la c'est comme dans truffle console.

Il faut bien penser aux ``await`` (pour eviter les objets ``Promise``) et utiliser une IP au lieu de localhost : ``var web3 = await new Web3('http://127.0.0.1:8544');``
- On installe ``ethers`` pour interagir avec la blockchain ``npm install --save-dev @nomiclabs/hardhat-ethers 'ethers@^5.0.0'`` (``--force`` possible si probleme de dependance ``mocha``) et on l'importe dans ``hardhat.config.ts`` avec
```js
import "@nomiclabs/hardhat-ethers";
```
- Pour le deploiement du token de test (FunToken, cf. infra)
```bash
npm install --save-dev @openzeppelin/contracts
```

## Transfer &#x2705;


```js
Web3 = require('Web3')
const web3 = await new Web3('http://127.0.0.1:8545')
var theblock = await web3.eth.getBlock('latest')
var thetx = await web3.eth.getTransaction("xxx")
await web3.hexToAscii(thetx.input)
await token.transfer(owner.address, 500, {gasLimit:10000000,gasPrice:743444434})
// {gasLimit:3e7}
```

La classique fonctionne (envoie 1 ETH)
```js
await web3.eth.sendTransaction({from : '0x70997970C51812dc3A010C7d01b50e0d17dc79C8', to : '0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266', value : '1000000000000000000'});
```

- Hardhat upgrades et openzeppelin
```js
import "@openzeppelin/hardhat-upgrades";
yarn add
npm install --save-dev @openzeppelin/contracts
npm install --save-dev @openzeppelin/hardhat-upgrades
npm install --save-dev @nomiclabs/hardhat-ethers ethers
```

- On peut creer un token, le transferer avec cli ou MetaMask (cf. https://medium.com/@kaishinaw/erc20-using-hardhat-a-comprehensive-guide-3211efba98d4)
- The ``ethers.js`` library aims to be a complete and compact library for interacting with the Ethereum Blockchain and its ecosystem
-	A ``Provider`` (in ethers) is a class which provides an abstraction for a connection to the Ethereum Network. It provides read-only access to the Blockchain and its status.	 
-	A ``Signer`` is a class which (usually) in some way directly or indirectly has access to a private key, which can sign messages and transactions to authorize the network to charge your account ether to perform operations.	 
-	A ``Contract`` is an abstraction which represents a connection to a specific contract on the Ethereum Network, so that applications can use it like a normal JavaScript object.
- A function that allows an inheriting contract to override its behavior will be marked at ``virtual``. The function that overrides that base function should be marked as ``override``.
> public - all can access - can be used when contract was deployed, can be used in inherited contract

> external - Cannot be accessed internally, only externally - can be used when contract was deployed , can NOT be used in inherited contract

> internal - only this contract and contracts deriving from it can access - can NOT be used when contract was deployed , can be used in inherited contract

> private - can be accessed only from this contract - can NOT be used when contract was deployed, can NOT be used in inherited contract

> private is a subset of internal and external is a subset of public.

> totalSupply: A method that defines the total supply of your tokens, When this limit is reached the smart contract will refuse to create new tokens.

> balanceOf: A method that returns the number of tokens a wallet address has.

> transfer: A method that takes a certain amount of tokens from the total supply and gives it to a user.

> transferFrom: Another type of transfer method which is used to transfer tokens between users.

> approve: This method verifies whether a smart contract is allowed to allocate a certain amount of tokens to a user, considering the total supply.

> allowance: This method is exactly the same as the approved method except that it checks if one user has enough balance to send a certain amount of tokens to another.


- Script utilise : ``test-FunToken.js``
- bien penser a ajouter le deploiement du token au script ``deploy-uniswap.js``

### Metamask
- Default chain id 31337
- On ajoute le network
```
http://127.0.0.1:8545
node id : 1337
```
- On importe l'adresse du token qu'on a deploye
- pour le moment, on peut envoyer en utilisant l'option envoyer de MetaMask situee au niveau du token (pas celle en haut)

## AddLiquidity &#x2705;

- creation de deux tokens, transferts possibles avec MetaMask
- recuperer le token deja cree
```js
const tokenA = await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','0x8A791620dd6260079BF849Dc5567aDC3F2FdC318')
const tokenB =  await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','0x610178dA211FEF7D417bC0e6FeD39F05609AD788')
```
- operateur ternaire en js/ts
```js
const token0 = tokenA.address === token0Address ? tokenA : tokenB;
```
equivalent a
```
if(tokenA.address === token0Address){tokenA}else{tokenB}
```
- recuperer la paire
- Transferer des deux tokens a la paire et mint
- Pour mint bien utiliser ``expandTo18Decimals()`` (depuis ``import { expandTo18Decimals } from "../src/test/shared/utilities";``) pour definir les montants afin ne pas avoir de probleme : *Error: cannot estimate gas; transaction may fail or may require manual gas limit*
- mint (``UniswapV2Pair.sol``):
    - si totalSupply est nul, sqrt(amount1*amount2)
    - sinon min de amount0*totalSupply / reserve0 (resp. 1)
- Script utilise : ``test-Pair.ts``

## removeLiquidity &#x2705;

- j'ai ajoute une fonction ``shortenTo18Decimals`` dans ``../src/test/shared/utilities`` pour l'affichage des balances pour plus de lisibilite
- a chaque burn on ``.sub(MINIMUM_LIQUIDITY)`` pour ne pas tomber a 0 dans la pair.balance
- owner transfere transfere le montant a retirer a la paire puis on burn le montant et envoie les tokens a owner wallet
- burn (``UniswapV2Pair.sol``): montant a retirer * balanceOf / totalSupply (pour les deux tokens)

## Raw Swap &#x2705; **>>** &#x1F6A7;

- On transfere du token A a la paire
- Le swap renvoie du token B
- fonctionne avec la fonction ``swap`` de ``UniswapV2Pair.sol`` **en revanche il faut specifier le taux de change?** [a revoir]
- Script utilise : ``test-Swap.ts``
- on peut calculer le taux de change manuellement. à améliorer

## Router Swap &#x2705;

- Swap pour owner wallet
- Script utilise : ``test-Swap-Router.ts``

## Global &#x2705;
Pour owner et other wallets:
- transfer
- addLiquidity (avec ou sans router)
- removeLiquidity
- swap
- bien faire attention aux ``approval`` et ``allowance`` (cf. https://docs.openzeppelin.com/contracts/2.x/api/token/erc20#IERC20-transferFrom-address-address-uint256-)
- Script utilise : ``test-Global-Router.ts``

## Simulation  &#x2705; **>>** &#x1F6A7;

- Script utilise : ``test-Global-simu.ts``
- ajout du module pour le csv ``yarn add csv-parse``
- fichier *.csv* à placer dans le même repertoire que le script
- ajout du module pour la sortie image ``npm install --save-dev chartjs-node-canvas``
- sort un fichier ``out.png``
- ajout du module pour la sortie .csv ``npm install --save-dev csv-stringify``
- png non interactif de qualité médiocre, et en plus c'est très lent! ~40min pour les 6397 transactions
- à améliorer

### autres js

```js
// an anonymous function
(function () { console.log('allo') });
// a self invoked anonymous function
(function () { console.log('allo') })(); //print
// a self invoked anonymous function with a parameter called "$"
var jQuery = 'I\'m not jQuery.'; //print
(function ($) { console.log($) })(jQuery);
```


# Pour la version truffle de UniswapV2

## Installation &#x2705;

- decommenter dans `truffle-config.js` le network: meme port que `ganache-cli` + all networks avec `*`
- creer un fichier `./common/mnemonic.data` vide
- need `npm install @truffle/hdwallet-provider`
- erreur nb d'arguments truffle test: il faut ajouter une adresse checksum dans le `migration.js`
```js
require("web3")
Web3.utils.toChecksumAddress("adresse_fournie_par_ganache")
```
comme 2eme argument
- Error: Could not find artifacts for Migrations from any source: ajouter un fichier `Migrations.sol` genere automatiquement par truffle init dans le dossier contracts

## Deploy &#x1F6A7; **>>** &#x274E; ?

rien n'a changé

- On peut utiliser ``node``
mais aussi ``truffle console`` :

```js
var Web3 = require('web3');
var web3 = await new Web3('http://localhost:8545');
web3.eth.getAccounts();
var accounts = await web3.eth.getAccounts()
web3.eth.getBalance(accounts[0]).then(console.log)
(await web3.eth.getBalance(accounts[0])).toString()
```
- Instancier le smartcontract :
```js
UniswapV2Factory.deployed().then(function(instance){ dapp = instance; })
```
*alternative à tester :*
```js
let dapp = await UniswapV2Factory.deployed()
```

- ``dapp.allPairs.length`` renvoie 0 (normal pour le moment)

- https://github.com/Uniswap/v2-periphery/blob/master/contracts/libraries/UniswapV2Library.sol#L35 function ``quote`` pour determiner le nombre de jetons a renvoyer quand on en envoie

- Note : ```Au moment d'écrire ces lignes, il y a 388.160 jetons ERC-20. S'il y avait un échange de paires pour chaque paire de jetons, cela représenterait plus de 150 milliards d'échanges de paires. La chaîne entière, en cet instant précis, ne dispose que de 0,1 % de ce nombre de comptes. Les fonctions d'échange supportent le concept du chemin. Un trader peut échanger A contre B, B contre C et C contre D. Ainsi, il n'y a pas besoin d'un échange direct de paire A-D.```

- La suite des etapes semble etre (cf. https://ethereum.stackexchange.com/questions/89993/uniswap-on-ganache)
> - Deploy the UniswapExchange contract
> - Deploy the UniswapFactory contract
> - Initialize the UniswapFactory (with the UniswapExchange address)
> - Deploy the Dai ERC20 token contract
> - Deploy the Dai UniswapExchange contract (using the Dai address)
> - Approve the Dai UniswapExchange contract to transfer Dai
> - Add liquidity to the Dai UniswapExchange contract

mais ca semble tres penible (cf aussi https://forum.openzeppelin.com/t/how-to-deploy-uniswapv2-on-ganache/3885) de faire fonctionner tout le pipeline.

**A suivre...**

## Metamask
Default chain id 1337
