import { HardhatUserConfig } from "hardhat/config";

import "@nomicfoundation/hardhat-toolbox";

import * as dotenv from "dotenv";
dotenv.config();

// tasks
import "./src/tasks/accounts";

// import deploy plugin https://github.com/AlexBHarley/uniswap-v2-deploy-plugin
import "uniswap-v2-deploy-plugin";

// import ethers pour interagir avec la blockchain
import "@nomiclabs/hardhat-ethers";

// import openzeppelin pour les token ERC20
import "@openzeppelin/hardhat-upgrades";
//import "@openzeppelin/contracts/token/ERC20/ERC20.sol";


const config: HardhatUserConfig = {
  networks: {
    hardhat: {
      blockGasLimit: 30000000,
//      chainId: 1337,
      allowUnlimitedContractSize: true,
    },
    localhost: {
//      url: "http://127.0.0.1:8544",
//      accounts: ['0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80'],
      allowUnlimitedContractSize: true,
    },
  },
  solidity: {
    version: "0.8.4",
    settings: {
      optimizer: {
        enabled: true,
        runs: 9999,
      },
      metadata: {
        bytecodeHash: "none",
      },
    },
  },
  gasReporter: {
    enabled: process.env.REPORT_GAS !== undefined,
    currency: "USD",
  },
  typechain: {
    outDir: "typechain-types",
    target: "ethers-v5",
  },
  paths: {
    tests: "./src/test",
  },
};

export default config;
