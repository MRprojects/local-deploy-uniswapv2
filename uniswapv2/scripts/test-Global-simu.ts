const { ethers } = require("hardhat");
import { UniswapV2Pair, ERC20 } from "../typechain-types";
import { BigNumber, Contract } from 'ethers'
//import { bigNumberify } from 'ethers'
import { expandTo18Decimals, encodePrice, shortenTo18Decimals } from "../src/test/shared/utilities";
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');
const MINIMUM_LIQUIDITY = BigNumber.from(10).pow(3);

/********************************************************************
*********************************************************************
    Plot Chart using chartjs
    npm install --save-dev chartjs-node-canvas
*********************************************************************
********************************************************************/
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');

const width = 2400; //px
const height = 800; //px
const backgroundColour = 'white'; // Uses https://www.w3schools.com/tags/canvas_fillstyle.asp
const chartJSNodeCanvas = new ChartJSNodeCanvas({ width, height, backgroundColour });

var configuration = {
  type: 'line',   // for line chart
  data: {
      labels: <any[]> [],
      datasets: [{
          label: "Pool WETH/USDC",
          data: <any[]> [],
          fill: false,
          borderColor: ['rgb(51, 204, 204)'],
          borderWidth: 1,
          xAxisID: 'xAxis1', //define top or bottom axis ,modifies on scale
          pointStyle: 'dash',
      },
      ],
  },
  options: {
      scales: {
          y: {
              suggestedMin: 1490,
              suggestedMax: 1570,
          }
      }
  }
}

async function drawChart() {
    const dataUrl = await chartJSNodeCanvas.renderToDataURL(configuration);
    const base64Image = dataUrl
    var base64Data = base64Image.replace(/^data:image\/png;base64,/, "");
    fs.writeFile("./scripts/out.png", base64Data, 'base64', function (err) {
        if (err) {
            console.log(err);
        }
    });
    return dataUrl
}


/********************************************************************
*********************************************************************
    Parse CSV file using csv-parse
*********************************************************************
********************************************************************/


import * as fs from "fs";
import * as path from "path";
import { parse } from 'csv-parse';

type Transactions = {
  timestamp: string;
  type: string;
  user: string;
  amountUSDC: number;
  amountWETH: number;
};

var Result

(() => {

  console.log(`=======> sync function read CSV\n`);

  const csvFilePath = path.resolve(__dirname, 'USDC_WETH_pool.csv');

  const headers = ['timestamp', 'type', 'user', 'amountUSDC', 'amountWETH'];

  const fileContent = fs.readFileSync(csvFilePath, { encoding: 'utf-8' });

  
  parse(fileContent, {
    delimiter: ',',
    columns: headers,
/*     on_record: (line, context) => {
      if (line.user == '') { // les mint/burn
      //if (line.type === "swap" || line.type === "type" ) { // les mint/burn et pas la premiere ligne
      //if (line.amountUSDC < 100002 && line.amountUSDC > -100002) { // une dizaine
        return;
      }
      return line;
    }, */
  }, (error, result: Transactions[]) => {
    if (error) {
      console.error(error);
    }
    //console.log("Result", result);
    console.log(`result.length in parse function : ${(result.length)} \n`);
    Result = result;
  });
})();


/********************************************************************
*********************************************************************
    Parameters to write CSV file using csv-stringify
*********************************************************************
********************************************************************/

const { stringify } = require("csv-stringify");
const filename = "./scripts/ExRate_WETHUSDC.csv";
const writableStream = fs.createWriteStream(filename);

const columns = [
  "timestamp",
  "Exchange Rate WETH/USDC",
];

/********************************************************************
*********************************************************************
    Usual main : Deploy Uniswap and perform transactions
*********************************************************************
********************************************************************/

async function main() {


/********************************************************************
*********************************************************************
    Deploy Uniswap
*********************************************************************
********************************************************************/

  //const [wallet, other1, other2] = await ethers.getSigners();
  const signers = await ethers.getSigners()
  const wallet = signers[0];
  const token = await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20");
  //console.log([wallet, other1, other2]);

  //console.log(`=======> async main\n`);
  // console.log("Result", Result);
  //console.log(`Result.length in async function : ${(Result.length)} \n`);

  // console.log(`Result[4937] : ${(await Result[4937])} \n`); // string interpolation : will return [object Object]
  // console.log("Result[4937] : ", Result[4937], "\n");
  let random = Math.floor(Math.random() * Result.length);
  console.log("here is a random transaction, number ",random," : ", Result[random], "\n");

  // deploy tokens
  console.log('===> Deploy two tokens...\n');
  const tokenA = await token.deploy(expandTo18Decimals(100000000)); // 100 millions
  const addressA = await tokenA.address;
  console.log(`Token A Address: ${addressA}`);
  console.log(`Total Supply in Token A: ${ethers.utils.formatUnits((await tokenA.totalSupply()), (await tokenA.decimals()))}\n`);
  const tokenB = await token.deploy(expandTo18Decimals(100000000));
  const addressB = await tokenB.address;
  console.log(`Token B Address: ${addressB}`);
  console.log(`Total Supply in Token B: ${ethers.utils.formatUnits((await tokenB.totalSupply()), (await tokenB.decimals()))}\n`);

  const weth = await ethers.getContractFactory("WETH9");
  const WEETH = await weth.deploy();

  //const erc20 = await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20");
  //const WETHPartner = await erc20.deploy(expandTo18Decimals(10000));

  // deploy V2
  console.log('===> Getting the Factory contract...\n');
  const v2factory = await ethers.getContractFactory("UniswapV2Factory");
  const factoryV2 = await v2factory.deploy(wallet.address);

  const routerEmit = await ethers.getContractFactory("RouterEventEmitter");

  const RouterEmit = await routerEmit.deploy();

  // deploy routers
  console.log('===> Getting the Router contract...\n');
  const router = await ethers.getContractFactory("UniswapV2Router");
  const router02 = await router.deploy(factoryV2.address, WEETH.address);

  // initialize V2
  console.log('===> Create Pair...\n');
  await factoryV2.createPair(tokenA.address, tokenB.address);
  const pairAddress = await factoryV2.getPair(tokenA.address, tokenB.address);
  const pairFactory = await ethers.getContractFactory("UniswapV2Pair");
  const pair = new Contract(
    pairAddress,
    pairFactory.interface,
    wallet
  ) as UniswapV2Pair;

  var USDC;
  var WETH;

  console.log('===> Determine who\'s who...\n');
  const token0Address = await pair.token0();
  const token0 = tokenA.address === token0Address ? tokenA : tokenB;
  //const USDCtest = tokenA.address === token0Address ? tokenA : tokenB;
  tokenA.address === token0Address ? (console.log("TokenA (USDC) is"), USDC = token0 ) : (console.log("TokenB (WETH) is"), WETH = token0 ) ;
  console.log(`Token0 Address: ${token0.address}`);
  const token1 = tokenA.address === token0Address ? tokenB : tokenA;
  //const WETHtest = tokenA.address === token0Address ? tokenB : tokenA;
  tokenA.address === token0Address ? (console.log("TokenB (WETH) is"), WETH = token1 ) : (console.log("TokenA (USDC) is"), USDC = token1 ) ;
  console.log(`Token1 Address: ${token1.address}\n`);


  console.log(`Token USDC Address: ${USDC.address}`); 
  console.log(`Token WETH Address: ${WETH.address}\n`); 
  //console.log(`Token USDC test Address: ${USDCtest.address}`); 
  //console.log(`Token WETH test Address: ${WETHtest.address}\n`); 


/********************************************************************
*********************************************************************
    Initialize Pool
*********************************************************************
********************************************************************/


  console.log('===> Initial Mint (set pool to initial state)...\n');
  const floatUSDC = 43717959.978289;
  const initialUSDCAmount = ethers.utils.parseEther(floatUSDC.toFixed(18))
  console.log(`USDC initial amount : ${floatUSDC} ( ${initialUSDCAmount} )`); 
  const floatWETH = 28316.506300133937;
  const initialWETHAmount = ethers.utils.parseEther(floatWETH.toFixed(18))
  console.log(`WETH initial amount : ${floatWETH} ( ${initialWETHAmount} )\n`); 

  await token0.approve(router02.address, ethers.constants.MaxUint256);
  await token1.approve(router02.address, ethers.constants.MaxUint256);
  router02.addLiquidity(
      USDC.address,
      WETH.address,
      initialUSDCAmount,
      initialWETHAmount,
      0,
      0,
      wallet.address,
      ethers.constants.MaxUint256
  )

  console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
  console.log("Pair Total Supply: ", parseFloat(ethers.utils.formatEther(await pair.totalSupply())));
  console.log("Owner Pair Balance: ", parseFloat(ethers.utils.formatEther(await pair.balanceOf(wallet.address))));
  console.log("Pair Token1 Balance: ", parseFloat(ethers.utils.formatEther(await token1.balanceOf(pair.address))));
  console.log("Pair Token0 Balance: ", parseFloat(ethers.utils.formatEther(await token0.balanceOf(pair.address))));
  console.log("Owner Token1 Balance: ", parseFloat(ethers.utils.formatEther(await token1.balanceOf(wallet.address))));
  console.log("Owner Token0 Balance: ", parseFloat(ethers.utils.formatEther(await token0.balanceOf(wallet.address))), "\n");


/********************************************************************
*********************************************************************
    Dispatch tokens
*********************************************************************
********************************************************************/

  console.log(`===> Transfers from owner to others...`);

  const initialOtherUSDCAmount = expandTo18Decimals(Math.floor(56282040/20));
  const initialOtherWETHAmount = expandTo18Decimals(Math.floor(99971683/20));
  for (const signer of signers) { 
    //console.log(`===> Transfer to ${signer.address}...`);
    tokenA.address === token0Address 
        ? (
        //console.log("transfer USDC "), 
        await token0.transfer(signer.address, initialOtherUSDCAmount) ) 
        : ( //console.log("transfer WETH "), 
        await token0.transfer(signer.address, initialOtherWETHAmount) ) ;
    tokenA.address === token0Address 
        ? (
        //console.log("transfer WETH "), 
        await token1.transfer(signer.address, initialOtherWETHAmount) ) 
        : ( //console.log("transfer USDC "), 
        await token1.transfer(signer.address, initialOtherUSDCAmount) ) ;
    //await token0.transfer(signer.address, initialOtherUSDCAmount);
    //await token1.transfer(signer.address, initialOtherWETHAmount);
    //console.log(`Other Signer Token1 Balance: ${await token1.balanceOf(signer.address)} ( ${shortenTo18Decimals(await token1.balanceOf(signer.address))} ) ( ParseFloat: ${parseFloat(ethers.utils.formatEther(await token1.balanceOf(signer.address)))} )`);
    //console.log(`Other Signer Token0 Balance: ${await token0.balanceOf(signer.address)} ( ${shortenTo18Decimals(await token0.balanceOf(signer.address))} ) ( ParseFloat: ${parseFloat(ethers.utils.formatEther(await token0.balanceOf(signer.address)))} )\n`);  
    
    // approve router once and for all
    await token0.connect(signer).approve(router02.address, ethers.constants.MaxUint256);
    await token1.connect(signer).approve(router02.address, ethers.constants.MaxUint256);
    //console.log(`allowance router over other's token0 wallet: ${(await token0.connect(signers[swapSigner]).allowance(signers[swapSigner].address, router02.address))}`);
    //console.log(`allowance router over other's token1 wallet: ${(await token1.connect(signers[swapSigner]).allowance(signers[swapSigner].address, router02.address))}\n`);
  }

  console.log(`Owner Token1 Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} ) ( ParseFloat: ${parseFloat(ethers.utils.formatEther(await token1.balanceOf(wallet.address)))} )`);
  console.log(`Owner Token0 Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} ) ( ParseFloat: ${parseFloat(ethers.utils.formatEther(await token0.balanceOf(wallet.address)))} )\n`);



/*   console.log(`===> Test random Signers...\n `);
  console.log(`In total we have ${signers.length} signers\n`);
  let countSame = 0;
  for (let i = 0; i <100; i++){
    // randomly choose two signers
    let pairSigner1 = Math.floor(Math.random() * signers.length);
    console.log("Random Signer[",pairSigner1,"] : ", signers[pairSigner1].address, "");
    let pairSigner2 = Math.floor(Math.random() * signers.length);
    while(pairSigner1 == pairSigner2)
    {
      pairSigner2 = Math.floor(Math.random() * signers.length);
      console.log("NOOOOOOOOO\n");
    }
    console.log("Random Signer[",pairSigner2,"] : ", signers[pairSigner2].address, "\n");
    // must be different addresses
    if (pairSigner1 == pairSigner2){
      console.log("should not be printed\n");
      countSame++;
    }
  }
  console.log(`we got ${countSame} time(s) a singleton (should be 0)\n`);
 */


/********************************************************************
*********************************************************************
    Process transactions from Result
*********************************************************************
********************************************************************/

  let noMoreTxPls = 0;
  let countMBT = 0;
  let countError = 0;
  let countInvalidTx = 0;
  let countBurn = 0;
  let countMint = 0;
  let countSwap = 0;
  const stringifier = stringify({ header: true, columns: columns });

  for (var theTransaction of Result){
    if (theTransaction.type !== 'swap'){
      //console.log(theTransaction);
      countMBT++;
    }

    switch (theTransaction.type) {
      /**********************************
          CASE : Swap
      **********************************/
      case 'swap':
        countSwap++;
        
        // parse and add date to plot
        let thetimestmp = (new Date(Date.parse(theTransaction.timestamp))).toLocaleString("en-GB");
        console.log("==>", thetimestmp );
        configuration.data.labels.push( thetimestmp );

        // compute exchange rate and add to plot
        let reserves = await pair.getReserves();
        let exchRate;
        USDC.address === token0Address 
          ? (
          // USDC : 0 & WETH : 1
          exchRate = parseFloat(ethers.utils.formatEther(reserves[0])) / parseFloat(ethers.utils.formatEther(reserves[1])) )
          : (
          // USDC : 1 & WETH : 0
          exchRate = parseFloat(ethers.utils.formatEther(reserves[1])) / parseFloat(ethers.utils.formatEther(reserves[0])) )
        console.log("Exchange rate: ", exchRate);
        configuration.data.datasets[0].data.push(exchRate);

        // add date and exchange rate to csv stream
        stringifier.write([thetimestmp,exchRate]);

        // get random signer
        let swapSigner = Math.floor(Math.random() * signers.length);
        // determine swap direction
        let swapUSDCAmount = parseFloat(theTransaction.amountUSDC);
        let swapWETHAmount = parseFloat(theTransaction.amountWETH);
        if(swapUSDCAmount < 0 && swapWETHAmount > 0) {
          console.log(`swap WETH for USDC`);
          console.log("USDC: ", swapUSDCAmount, " and WETH: ", swapWETHAmount);
          await router02.connect(signers[swapSigner]).swapExactTokensForTokens(
              ethers.utils.parseEther(swapWETHAmount.toFixed(18)),
              0,
              [WETH.address, USDC.address], //swap WETH for USDC
              signers[swapSigner].address,
              ethers.constants.MaxUint256
          )
        }
        else if(swapUSDCAmount > 0 && swapWETHAmount < 0) {
          console.log(`swap USDC for WETH`);
          console.log("USDC: ", swapUSDCAmount, " and WETH: ", swapWETHAmount);
          await router02.connect(signers[swapSigner]).swapExactTokensForTokens(
              ethers.utils.parseEther(swapUSDCAmount.toFixed(18)),
              0,
              [USDC.address, WETH.address], //swap USDC for WETH
              signers[swapSigner].address,
              ethers.constants.MaxUint256
          )
        }
        else {
          console.log(`incorrect swap amounts`);
          //console.log(theTransaction); // swap -0.0 for 0.0
          countInvalidTx++;
          break; // useless?
        }
        break;
      /**********************************
          CASE : Burn
      **********************************/
      case 'burn':
        countBurn++;
        console.log("==>",Date.parse(theTransaction.timestamp));
        console.log("from:", theTransaction.user, "==> burn");
        let burnUSDCAmount = parseFloat(theTransaction.amountUSDC);
        let burnWETHAmount = parseFloat(theTransaction.amountWETH);
        if(burnUSDCAmount <= 0 || burnWETHAmount <= 0) {
          console.log(`error: receiving zero or negative amounts`);
          countInvalidTx++;
          break;
        }
        console.log("USDC: ", burnUSDCAmount, " and WETH: ", burnWETHAmount);
        let expectedLiquidity0;
        let expectedLiquidity1;

        // calcul des LP-shares nécessaires à partir de la formule amount0 = (liquidity * balance0) / _totalSupply; de la fonction burn de UniswapV2Pair.sol
        tokenA.address === token0Address ? ( expectedLiquidity0 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnUSDCAmount.toFixed(18))).div(await token0.balanceOf(pair.address)) ) : ( expectedLiquidity0 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnWETHAmount.toFixed(18))).div(await token0.balanceOf(pair.address)) ) ;
        tokenA.address === token0Address ? ( expectedLiquidity1 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnWETHAmount.toFixed(18))).div(await token1.balanceOf(pair.address)) ) : ( expectedLiquidity1 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnUSDCAmount.toFixed(18))).div(await token1.balanceOf(pair.address)) ) ;
        //let expectedLiquidity = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnUSDCAmount.toFixed(18))).div(await token0.balanceOf(pair.address));
        //let expectedLiquidity1 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnWETHAmount.toFixed(18))).div(await token1.balanceOf(pair.address));
        //let expectedLiquidity2 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnUSDCAmount.toFixed(18))).div(await token1.balanceOf(pair.address));
        //let expectedLiquidity3 = (await pair.totalSupply()).mul(ethers.utils.parseEther(burnWETHAmount.toFixed(18))).div(await token0.balanceOf(pair.address));

        // expected liquidity from both amount and reserves
        //console.log("Expected liquidity:", parseFloat(ethers.utils.formatEther(expectedLiquidity0)));
        //console.log("Expected liquidity:", parseFloat(ethers.utils.formatEther(expectedLiquidity1)));

        // mean expected liquidity
        let expectedLiquidity = (expectedLiquidity0.add(expectedLiquidity1)).div(2);
        console.log("Mean Expected liquidity:", parseFloat(ethers.utils.formatEther(expectedLiquidity)));
        //let burnSigner = Math.floor(Math.random() * signers.length);
        //await pair.connect(signers[burnSigner]).approve(router02.address, ethers.constants.MaxUint256);
        //await router02.connect(signers[burnSigner]).removeLiquidity(
    
        // tout passe par owner pour eviter d'avoir à vérifier si il y a assez de LP-shares
        await pair.approve(router02.address, ethers.constants.MaxUint256);
        await router02.removeLiquidity(
            token0.address,
            token1.address,
            expectedLiquidity.sub(MINIMUM_LIQUIDITY),
            0,
            0,
            wallet.address,
            ethers.constants.MaxUint256
        ) 
        break;
      /**********************************
          CASE : Mint
      **********************************/
      case 'mint':
        countMint++;
        console.log("==>",Date.parse(theTransaction.timestamp));
        console.log("from:", theTransaction.user, "==> mint");
        let mintSigner = Math.floor(Math.random() * signers.length);
        let mintUSDCAmount = parseFloat(theTransaction.amountUSDC);
        let mintWETHAmount = parseFloat(theTransaction.amountWETH);
        if(mintUSDCAmount <= 0 || mintWETHAmount <= 0) {
          console.log(`error: trying to mint zero or negative amounts`);
          countInvalidTx++;
          break;
        }
        console.log("USDC: ", mintUSDCAmount, " and WETH: ", mintWETHAmount);
        await token0.connect(signers[mintSigner]).approve(router02.address, ethers.constants.MaxUint256);
        await token1.connect(signers[mintSigner]).approve(router02.address, ethers.constants.MaxUint256);
        await router02.connect(signers[mintSigner]).addLiquidity(
            USDC.address,
            WETH.address,
            ethers.utils.parseEther(mintUSDCAmount.toFixed(18)),
            ethers.utils.parseEther(mintWETHAmount.toFixed(18)),
            0,
            0,
            signers[mintSigner].address,
            ethers.constants.MaxUint256
        )
        break;
      /**********************************
          CASE : headers
      **********************************/
      case 'type': // cas de la premiere ligne
        break;
      /**********************************
          CASE : other
      **********************************/
      default:
        console.log(`error, but dunno what kind`);
        countError++;
    }
    noMoreTxPls ++;
    console.log("==> Tx No ", noMoreTxPls);
    if (noMoreTxPls >30) break; // bound the number of transactions
  }
  

  /*******************
  * Plot data 
  *******************/

  drawChart()

  /*******************
  * Export CSV
  *******************/

  stringifier.pipe(writableStream);

  /*******************
  * Print more data 
  *******************/

  console.log("");
  console.log(`we got ${countSwap} swap(s)`);
  console.log(`we got ${countMBT} stuff that are not swap:`); // sum of burns and mints, minus first line
  console.log(`- ${countBurn} burn`);
  console.log(`- ${countMint} mint`);
  console.log(`we got ${countError} error(s)`);
  console.log(`we got ${countSwap+countBurn+countMint} readable transactions among the ${Result.length} transactions (should be ${Result.length}-1)`);
  console.log(`we got ${countInvalidTx} invalid transaction(s)\n`);


  console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)}`);
  console.log("Pair Total Supply: ", parseFloat(ethers.utils.formatEther(await pair.totalSupply())));
  console.log("Owner Pair Balance: ", parseFloat(ethers.utils.formatEther(await pair.balanceOf(wallet.address))));
  console.log("Pair Token1 Balance: ", parseFloat(ethers.utils.formatEther(await token1.balanceOf(pair.address))));
  console.log("Pair Token0 Balance: ", parseFloat(ethers.utils.formatEther(await token0.balanceOf(pair.address))));
  console.log("Owner Token1 Balance: ", parseFloat(ethers.utils.formatEther(await token1.balanceOf(wallet.address))));
  console.log("Owner Token0 Balance: ", parseFloat(ethers.utils.formatEther(await token0.balanceOf(wallet.address))), "\n");

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });


