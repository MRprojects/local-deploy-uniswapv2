const { ethers } = require("hardhat");
import { UniswapV2Pair, ERC20 } from "../typechain-types";
import { BigNumber } from 'ethers'
//import { bigNumberify } from 'ethers'
import { expandTo18Decimals, encodePrice, shortenTo18Decimals } from "../src/test/shared/utilities";
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');
const MINIMUM_LIQUIDITY = BigNumber.from(10).pow(3);

async function main() {

    const [wallet, other] = await ethers.getSigners();
    console.log('===> Getting the Factory contract...\n');
    const factoryAddress = '0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0'; // Factory address
    const factory = await ethers.getContractAt('UniswapV2Factory', factoryAddress);

    console.log(`Factory Address: ${factory.address}\n`);

    console.log('===> Deploying two tokens...\n');

    const tokenA = await (
        await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20")
      ).deploy(expandTo18Decimals(10000)) as ERC20;
    // si deja cree
    //const tokenA = await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','0x5FC8d32690cc91D4c39d9d3abcBD16989F875707')

    const addressA = await tokenA.address;
    console.log(`Token A Address: ${addressA}`);
    
    console.log('===> Deploying token A...\n');
    var name = await tokenA.name();
    console.log(`Token Name: ${name}`);
    var symbol = await tokenA.symbol();
    console.log(`Token Symbol: ${symbol}`);
    var decimals = await tokenA.decimals();
    console.log(`Token Decimals: ${decimals}\n`);
    
    console.log('===> Deploying token B...\n');

    const tokenB = (await (
          await ethers.getContractFactory("contracts/test/ERC20.sol:ERC20")
        ).deploy(expandTo18Decimals(10000))) as ERC20;
    // si deja cree
    //const tokenB =  await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','0x0165878A594ca255338adfa4d48449f69242Eb8F')


    const addressB = await tokenB.address;
    console.log(`Token B Address: ${addressB}`);  
    
    name = await tokenB.name();
    console.log(`Token Name: ${name}`);
    symbol = await tokenB.symbol();
    console.log(`Token Symbol: ${symbol}`);
    decimals = await tokenB.decimals();
    console.log(`Token Decimals: ${decimals}\n`);
    
    console.log('===> Create Pair token A / token B...\n');
  
    await factory.createPair(tokenA.address, tokenB.address);
    const pair = (await ethers.getContractFactory("UniswapV2Pair")).attach(
       await factory.getPair(tokenA.address, tokenB.address)
    );
    // si deja cree
    //const pairContract = await factory.getPair(tokenA.address, tokenB.address);
    //const pair = (await ethers.getContractFactory("UniswapV2Pair")).attach(pairContract);

    console.log(`Pair Address: ${pair.address}`);
    
    
    const token0Address = await pair.token0();
    const token0 = tokenA.address === token0Address ? tokenA : tokenB;
    tokenA.address === token0Address ? console.log("TokenA is") : console.log("TokenB is") ;
    console.log(`Token0 Address: ${token0.address}`);
    const token1 = tokenA.address === token0Address ? tokenB : tokenA;
    tokenA.address === token0Address ? console.log("TokenB is") : console.log("TokenA is") ;
    console.log(`Token1 Address: ${token1.address}\n`);
    
    let pairTSupply = await pair.totalSupply();
    console.log(`Pair Total Supply: ${pairTSupply}\n`);


    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other TokenA Balance: ${await token1.balanceOf(other.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other.address))} )`);
    console.log(`Other TokenB Balance: ${await token0.balanceOf(other.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other.address))} )\n`);



    console.log('===> Add Liquidity TokenA and TokenB...\n');

    console.log('=> Transfer...\n');

    const amountA = expandTo18Decimals(2);
    console.log(`Add: ${amountA} TokenA`);
    await tokenA.transfer(pair.address, amountA);
    const amountB = expandTo18Decimals(5);
    console.log(`Add: ${amountB} TokenB\n`);
    await tokenB.transfer(pair.address, amountB);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other TokenA Balance: ${await token1.balanceOf(other.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other.address))} )`);
    console.log(`Other TokenB Balance: ${await token0.balanceOf(other.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other.address))} )\n`);

    console.log('=> Mint...\n');
    await pair.mint(wallet.address);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other TokenA Balance: ${await token1.balanceOf(other.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other.address))} )`);
    console.log(`Other TokenB Balance: ${await token0.balanceOf(other.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other.address))} )\n`);



    console.log('===> Remove Liquidity TokenA and TokenB...\n');

    const expectedLiquidity = expandTo18Decimals(2);
    console.log(`Remove: ${expectedLiquidity}\n`);

    console.log('=> Transfer...\n');

    await pair.transfer(pair.address, expectedLiquidity.sub(MINIMUM_LIQUIDITY));

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other TokenA Balance: ${await token1.balanceOf(other.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other.address))} )`);
    console.log(`Other TokenB Balance: ${await token0.balanceOf(other.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other.address))} )\n`);


    console.log('=> Burn...\n');

    await pair.burn(wallet.address);

    console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )`);
    console.log(`Other TokenA Balance: ${await token1.balanceOf(other.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other.address))} )`);
    console.log(`Other TokenB Balance: ${await token0.balanceOf(other.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other.address))} )\n`);


}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });


