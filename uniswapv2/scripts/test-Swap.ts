const { ethers } = require("hardhat");
import { UniswapV2Pair, ERC20 } from "../typechain-types";
import { BigNumber } from 'ethers'
//import { bigNumberify } from 'ethers'
import { expandTo18Decimals, encodePrice, shortenTo18Decimals } from "../src/test/shared/utilities";
const Web3 = require('web3');
const web3 = new Web3('http://localhost:8545');
const MINIMUM_LIQUIDITY = BigNumber.from(10).pow(3);

async function main() {

    const [wallet, other] = await ethers.getSigners();
    console.log('===> Getting the Factory contract...\n');
    const factoryAddress = '0x9fE46736679d2D9a65F0992F2272dE9f3c7fa6e0'; // Factory address
    const factory = await ethers.getContractAt('UniswapV2Factory', factoryAddress);

    console.log(`Factory Address: ${factory.address}\n`);

    const tokenA = await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','0x5FC8d32690cc91D4c39d9d3abcBD16989F875707')
    const addressA = await tokenA.address;
    console.log(`Token A Address: ${addressA}`);
    
    const tokenB =  await ethers.getContractAt('contracts/test/ERC20.sol:ERC20','0x0165878A594ca255338adfa4d48449f69242Eb8F')
    const addressB = await tokenB.address;
    console.log(`Token B Address: ${addressB}`);  
    
    const pairContract = await factory.getPair(tokenA.address, tokenB.address);
    const pair = (await ethers.getContractFactory("UniswapV2Pair")).attach(pairContract);

    console.log(`Pair Address: ${pair.address}`);
    
    const token0Address = await pair.token0();
    const token0 = tokenA.address === token0Address ? tokenA : tokenB;
    tokenA.address === token0Address ? console.log("TokenA is") : console.log("TokenB is") ;
    console.log(`Token0 Address: ${token0.address}`);
    const token1 = tokenA.address === token0Address ? tokenB : tokenA;
    tokenA.address === token0Address ? console.log("TokenB is") : console.log("TokenA is") ;
    console.log(`Token1 Address: ${token1.address}\n`);
    
    let pairTSupply = await pair.totalSupply();
    console.log(`Pair Total Supply: ${pairTSupply}\n`);


    let reserves = await pair.getReserves(); 
    console.log(`Reserve A : ${reserves[1]} ( ${shortenTo18Decimals(reserves[1])} )`);
    console.log(`Reserve B : ${reserves[0]} ( ${shortenTo18Decimals(reserves[0])} )`);

    const totalSupplyToken0 = await token0.totalSupply();
    console.log(`Total supply Token0 : ${totalSupplyToken0} ( ${shortenTo18Decimals(totalSupplyToken0)} )`);
    const totalSupplyToken1 = await token1.totalSupply();
    console.log(`Total supply Token1 : ${totalSupplyToken1} ( ${shortenTo18Decimals(totalSupplyToken1)} )\n`);



    //console.log(`Pair Pair Balance: ${await pair.balanceOf(pair.address)} ( ${shortenTo18Decimals(await pair.balanceOf(pair.address))} )`);
    //console.log(`Pair Total Supply: ${await pair.totalSupply()} ( ${shortenTo18Decimals(await pair.totalSupply())} )`);
    //console.log(`Owner Pair Balance: ${await pair.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await pair.balanceOf(wallet.address))} )`);
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);
    //console.log(`Other TokenA Balance: ${await token1.balanceOf(other.address)} ( ${shortenTo18Decimals(await token1.balanceOf(other.address))} )`);
    //console.log(`Other TokenB Balance: ${await token0.balanceOf(other.address)} ( ${shortenTo18Decimals(await token0.balanceOf(other.address))} )\n`);

    console.log('===> Swap token A for token B...\n');

    console.log('=> Transfer...\n');

    const swapAmountA = expandTo18Decimals(3);
    await token1.transfer(pair.address, swapAmountA);
    console.log(`Transfer ${swapAmountA} Token A \n`);

    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('=> Swap...\n');

    //const expectedOutputAmount = BigNumber.from("453305446940074565");
    const expectedAmountB = expandTo18Decimals(1);
    await pair.swap(expectedAmountB, 0, wallet.address, "0x");
    console.log(`Swap for ${expectedAmountB} Token B \n`);

    
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('===> Swap token B for token A...\n');

    console.log('=> Transfer...\n');

    const swapAmountB = expandTo18Decimals(3);
    await token0.transfer(pair.address, swapAmountB);
    console.log(`Transfer ${swapAmountB} Token B \n`);

    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);

    console.log('=> Swap...\n');

    //const expectedOutputAmount = BigNumber.from("453305446940074565");
    const expectedAmountA = expandTo18Decimals(1);
    await pair.swap(0, expectedAmountA, wallet.address, "0x");
    console.log(`Swap for ${expectedAmountA} Token A \n`);

    
    console.log(`Pair TokenA Balance: ${await token1.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token1.balanceOf(pair.address))} )`);
    console.log(`Pair TokenB Balance: ${await token0.balanceOf(pair.address)} ( ${shortenTo18Decimals(await token0.balanceOf(pair.address))} )`);
    console.log(`Owner TokenA Balance: ${await token1.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token1.balanceOf(wallet.address))} )`);
    console.log(`Owner TokenB Balance: ${await token0.balanceOf(wallet.address)} ( ${shortenTo18Decimals(await token0.balanceOf(wallet.address))} )\n`);


}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exitCode = 1;
    });


